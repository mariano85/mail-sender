package com.adistec.mailsender.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.adistec.mailsender.interceptor.MailSenderInterceptor;

@Configuration
public class WebMvcConfig implements WebMvcConfigurer {
	
	@Bean
	MailSenderInterceptor mailSenderInterceptor() {
		return new MailSenderInterceptor();
	}

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(mailSenderInterceptor()).addPathPatterns("/**");
    }
}