package com.adistec.mailsender.configuration;

import java.util.Properties;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

@Configuration
public class MailSenderConfiguration {
	
	@Value("${mail.open.host}")
    private String openHost;

    @Value("${mail.open.port}")
    private Integer openPort;
    
    @Value("${mail.office.host}")
    private String officeHost;

    @Value("${mail.office.port}")
    private Integer officePort;
    
    @Value("${mail.office.username}")
    private String officeUsername;
    
    @Value("${mail.office.password}")
    private String officePassword;
    
	@Bean
	public JavaMailSender officeServer() {
		JavaMailSenderImpl javaMailSender = new JavaMailSenderImpl();
		javaMailSender.setHost(officeHost);
		javaMailSender.setPort(officePort);
		javaMailSender.setUsername(officeUsername);
		javaMailSender.setPassword(officePassword);
		javaMailSender.setJavaMailProperties(getOfficeProperties());
		return javaMailSender;
	}
	
	@Bean
	public JavaMailSender openServer() {
		JavaMailSenderImpl javaMailSender = new JavaMailSenderImpl();
		javaMailSender.setHost(openHost);
		javaMailSender.setPort(openPort);
		return javaMailSender;
	}
	
	private Properties getOfficeProperties() {
        Properties properties = new Properties();
        properties.setProperty("mail.smtp.auth", "true");
        properties.setProperty("mail.smtp.starttls.enable", "true");
        return properties;
    }

}
