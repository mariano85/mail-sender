package com.adistec.mailsender.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import com.adistec.mailsender.service.MailService;

public class MailSenderInterceptor extends HandlerInterceptorAdapter {

	@Autowired
	private MailService mailService;
	
	@Autowired
	private JavaMailSender officeServer;
	
	@Autowired
	private JavaMailSender openServer;
	
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
		
		if (request.getHeader("smtpServer") != null) {
			switch (request.getHeader("smtpServer")) {
	            case "open":  mailService.setMailSender(openServer);
	                     break;
	            default:  mailService.setMailSender(officeServer);
	                     break;
			}
		} else {
			mailService.setMailSender(officeServer);
		}
		return true;
	}
}
