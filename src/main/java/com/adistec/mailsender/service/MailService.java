package com.adistec.mailsender.service;


import com.adistec.mailsender.entity.Mail;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
import java.io.IOException;
import java.util.List;

import javax.mail.MessagingException;

@Service
public interface MailService {
	List<String> getTemplateFiles() throws IOException;
	void sendFeedback(String templateVariables, String appName, String subject, String application) throws MessagingException, IOException;
	void sendRecovery(String templateVariables, String subject, String username, String application) throws MessagingException, IOException;
	void sendEmailWithTemplate(String sentToEmail, String subject, String template, String from, String templateVariables, String inlines, String copyTo, String cco, String application) throws IOException, MessagingException;
	void setMailSender(JavaMailSender mailSender);
	void retryFailedMails(JavaMailSender officeServer, JavaMailSender openServer);
	void sendEmailWithContent(String[] sentToEmail, String subject, String content, String from,String inlines, String copyTo[], String cco[], String application) throws MessagingException, IOException;
	List<Mail> getMailsSent(String to, String application);
}
